#!/bin/sh

## Utility to detect changes in IP and update nameserver

NAMESERVER="$1"
SUBDOMAIN="$2"
DOMAIN="$3"
KEYFILE="$4"

if [ -z "$NAMESERVER" ] || [ -z "$SUBDOMAIN" ] || [ -z "$DOMAIN" ]; then
    echo "ERROR: Parameters insufficient."
    echo "You have to invoke check like this:"
    echo "   ./check.sh namserver.example.com subdomain example.com /path/to/nsupdate/keyfile"
    exit 1
fi

# Use the right XDG_CACHE_HOME
if [ ! -d "$XDG_CACHE_HOME" ]
then
    XDG_CACHE_HOME=~/.cache
fi

# Ephemeral file that saves the IP address
IPFILE="$XDG_CACHE_HOME/ipaddress"

updateip () {
    NEWIP="$1"
    echo "New IP Is $NEWIP"

    TMPFILE=$(mktemp)

    echo "server $NAMESERVER 53" > "$TMPFILE"
    echo "zone $DOMAIN" >> "$TMPFILE"
    echo "update delete $SUBDOMAIN.$DOMAIN" >> "$TMPFILE"
    echo "update add $SUBDOMAIN.$DOMAIN 300 IN A $NEWIP" >> "$TMPFILE"
    echo "send" >> "$TMPFILE"

    nsupdate -k "$KEYFILE" -v "$TMPFILE"

    echo "Sent this:"
    echo "---------------"
    cat "$TMPFILE"
    echo "---------------"

    echo "Cleaning up..."
    rm "$TMPFILE"
}


CURRENTIP=$(curl --silent ipv4.icanhazip.com)

if [ -z "$CURRENTIP" ]; then
    echo "Error in getting current IP"
    exit
fi

if [ -f "$IPFILE" ]
then
    PREVIOUSIP=$(cat "$IPFILE")
else
    PREVIOUSIP="NOTSET"
fi

if [ "$CURRENTIP" = "$PREVIOUSIP" ]
then
    echo "$(date): No IP Change."
else
    echo "IP has changed to $CURRENTIP"
    echo "Updating name server"
    updateip "$CURRENTIP"
    echo "Saving to file"
    echo "$CURRENTIP" > "$IPFILE"
fi
